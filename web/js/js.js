/***
	公共方法

***/
//接口地址
var URLS = {};
URLS.SERVER_URL = "http://xhj.icecn.net/jianpu/";
URLS.login = "login";//登录
URLS.listUser = "listUser";
URLS.insertUser = "insertUser";
URLS.updateUser = "updateUser";
URLS.detailUser = "detailUser";
URLS.upload = "upload";
URLS.deleteUser = "deleteUser";

URLS.listReply = "listReply";
URLS.deleteReply = "deleteReply";

URLS.listPost = "listPost";
URLS.detailPost = "detailPost";
URLS.insertPost = "insertPost";
URLS.insertPostChild = "insertPostChild";
URLS.deletePost = "deletePost";
URLS.updatePost = "updatePost";




//配置参数及方法
var yj = {
	//控制跳转   url格式:html/index.html"
	goPage:function(url){
		var str = location.pathname;
		var index = str.indexOf('html') + 4;
		var path  = str.substring(0,index);
		location.href =  location.protocol + '//' + location.host + path + url;
	},
	//取url参数
	getUrlParam:function(name){
		var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
        var r = window.location.search.substr(1).match(reg);
        if (r != null) return unescape(r[2]); return null;
	}
};
var user = JSON.parse(sessionStorage.getItem("backUserInfo"));

console.log(user);
var url_text = location.href;



$(function () {
	//控制菜单跳转
	$(".sidebar-menu").on('click','li[data-href]',function(){
		var href = $(this).data("href");
		yj.goPage(href);
	});
	//获得登录信息
	if(user==null&&url_text.indexOf('login.html') <=1){
		window.location.href ='../../../login.html';
	}else{
		$(".skin-blue .main-header .navbar").append("<div class='userinfo'>欢迎您，<span>"+user.nick_name+"</span>，<a id='logout'>退出登录</a></div>")
	}
	////账号管理
	$(".sidebar-menu li.treeview").eq(0).find(".treeview-menu").html("<li data-href='/AccountManagement/AccountUser.html'><a><i class='fa fa-circle-o'></i>用户列表</a></li>");
	
	$(".sidebar-menu li.treeview").eq(1).find(".treeview-menu").html("<li data-href='/pl/pl_list.html'><a><i class='fa fa-circle-o'></i>评论列表</a></li>");

	$(".sidebar-menu li.treeview").eq(2).find(".treeview-menu").html("<li data-href='/tuiwen/tuiwen.html'><a><i class='fa fa-circle-o'></i>荐文/心得列表</a></li>");
	
//	//首页配置管理
//	$(".sidebar-menu li.treeview").eq(1).find(".treeview-menu").html("<li data-href='/HomeConfiguration/HomeBanner.html'><a><i class='fa fa-circle-o'></i>banner图管理</a></li><li data-href='/HomeConfiguration/HomeNewsColumn.html'><a><i class='fa fa-circle-o'></i>资讯栏目管理</a></li><li data-href='/HomeConfiguration/HomeNewsList.html'><a><i class='fa fa-circle-o'></i>资讯内容列表</a></li><li data-href='/HomeConfiguration/HomeAdsPictures.html'><a><i class='fa fa-circle-o'></i>广告图管理</a></li><li data-href='/HomeConfiguration/HomeStudio.html'><a><i class='fa fa-circle-o'></i>直播间配置</a></li>");
//	//赛事管理
//	$(".sidebar-menu li.treeview").eq(2).find(".treeview-menu").html("<li data-href='/RaceManagement/RaceStatistics.html'><a><i class='fa fa-circle-o'></i>赛事数据统计</a></li><li data-href='/RaceManagement/RaceCreate.html'><a><i class='fa fa-circle-o'></i>创建赛事</a></li><li data-href='/RaceManagement/RaceList.html'><a><i class='fa fa-circle-o'></i>赛事列表</a></li><li data-href='/RaceManagement/RaceClanList.html'><a><i class='fa fa-circle-o'></i>战队管理</a></li>");
//	//代练管理
//	$(".sidebar-menu li.treeview").eq(3).find(".treeview-menu").html("<li data-href='/PowerLeveling/PowerLevelingStatistics.html'><a><i class='fa fa-circle-o'></i>代练数据统计</a></li><li data-href='/PowerLeveling/PowerLevelingOrder.html'><a><i class='fa fa-circle-o'></i>代练订单管理</a></li><li data-href='/PowerLeveling/PowerLevelingComment.html'><a><i class='fa fa-circle-o'></i>评论管理</a></li><li data-href='/PowerLeveling/PowerLevelingAccount.html'><a><i class='fa fa-circle-o'></i>代练账号管理</a></li><li data-href='/PowerLeveling/PowerLevelingComplaints.html'><a><i class='fa fa-circle-o'></i>举报&amp;投诉类型配置</a></li>");
//	//竞猜管理
//	$(".sidebar-menu li.treeview").eq(4).find(".treeview-menu").html("<li data-href='/QuizManagement/QuizStatistics.html'><a><i class='fa fa-circle-o'></i>竞猜数据统计</a></li><li data-href='/QuizManagement/QuizRace.html'><a><i class='fa fa-circle-o'></i>竞猜赛事管理</a></li><li data-href='/QuizManagement/QuizOrder.html'><a><i class='fa fa-circle-o'></i>竞猜订单管理</a></li><li data-href='/QuizManagement/QuizNew.html'><a><i class='fa fa-circle-o'></i>新建竞猜管理</a></li><li data-href='/QuizManagement/QuizThird.html'><a><i class='fa fa-circle-o'></i>赛事管理（三方）</a></li>");
//	 //财务管理
//	$(".sidebar-menu li.treeview").eq(5).find(".treeview-menu").html("<li data-href='/FinanceManagement/FinanceStatistics.html'><a><i class='fa fa-circle-o'></i>财务数据统计</a></li><li data-href='/FinanceManagement/FinanceRechargeActivities.html'><a><i class='fa fa-circle-o'></i>充值活动配置</a></li><li data-href='/FinanceManagement/FinanceRechargeRecord.html'><a><i class='fa fa-circle-o'></i>充值记录管理</a></li><li data-href='/FinanceManagement/FinanceGold.html'><a><i class='fa fa-circle-o'></i>金币消耗/获取记录</a></li><li data-href='/FinanceManagement/FinanceDiamond.html'><a><i class='fa fa-circle-o'></i>钻石消耗/获取记录</a></li><li data-href='/FinanceManagement/FinanceVirtual.html'><a><i class='fa fa-circle-o'></i>手动增加虚拟币</a></li><li data-href='/FinanceManagement/FinanceAuditVirtual.html'><a><i class='fa fa-circle-o'></i>审核增加虚拟货币</a></li><li data-href='/FinanceManagement/FinanceStatement.html'><a><i class='fa fa-circle-o'></i>财务对账单</a></li>");
//	//商城管理
//	$(".sidebar-menu li.treeview").eq(6).find(".treeview-menu").html("<li data-href='/MallManagement/MallStatistics.html'><a><i class='fa fa-circle-o'></i>商城数据统计</a></li><li data-href='/MallManagement/MallList.html'><a><i class='fa fa-circle-o'></i>商城列表</a></li><li data-href='/MallManagement/MallOrder.html'><a><i class='fa fa-circle-o'></i>订单管理</a></li>");
//    //消息通知
//	$(".sidebar-menu li.treeview").eq(7).find(".treeview-menu").html("<li data-href='/Message/MessagePush.html'><a><i class='fa fa-circle-o'></i>PUSH通知</a></li><li data-href='/Message/MessageNotice.html'><a><i class='fa fa-circle-o'></i>短信通知</a></li><li data-href='/Message/MessageList.html'><a><i class='fa fa-circle-o'></i>历史通知（PUSH+短信）</a></li>");
	//加载菜单
	//$.ajax({
//		url:'web.icecn.net.json',
//		type:'POST',
//		async:true,
//		data:{
//			userId: user.id
//		},
//		dataType: "json",
//		xhrFields:{withCredentials:true},
//		success: function(data){
//			if(data.code === 200 ){
//			  var a="";
//			  var d=data.t;
//			  if(d.length>0){
//				  for(var i=0;i<d.length;i++){
//					  if(d[i].children.length>0){
//						  var b="";
//						  for(var j=0;j<d[i].children.length;j++){
//							if(d[i].children[j].isChoice==true){
//							  b+="<li data-href='"+d[i].children[j].resUrl+"'><a><i class='fa fa-circle-o'></i>"+d[i].children[j].name+"</a></li>" 
//							}else{
//							  b+="<li data-href='"+d[i].children[j].resUrl+"' class='hide'><a><i class='fa fa-circle-o'></i>"+d[i].children[j].name+"</a></li>" 
//							}
//						  } 
//						  a+="<li class='treeview'><a><i class='fa fa-user'></i><span>"+d[i].name+"</span><span class='pull-right-container'> <i class='fa fa-angle-left pull-right'></i></span></a><ul class='treeview-menu'>"+b+"</ul></li>";
//					  }
//				  }
//				  $(".sidebar-menu").html(a)
//			  }else{
//				$("#modal-Permissions ul").html("<li>无数据</li>")
//			  }
//			}else{
//			   alert(data.msg)	
//			}
//		}
//   });
  //选中菜单
  $(".sidebar-menu li.treeview").eq($("body").attr("data-treeview")).addClass("active").find(".treeview-menu").children("li").eq($("body").attr("data-treeview-menu")).addClass("active");
	//单图片上传
	function fileupload(input,img,photoCover){
		var fileObj = document.getElementById(input).files[0];
		var form = new FormData;
	    form.append("file1",fileObj);
		$.ajax({
			url:URLS.SERVER_URL+URLS.upload,
			type:'POST',
			data:form,
			async: false,
					cache: false,
					contentType: false,
					processData: false,
					dataType: "json",
			success: function(data){
				console.log(data);
				var domian=data.image[0];
				$("."+img).html("<img src='"+domian+"'/>")
				$("#"+photoCover).val(domian);
			}
	    });
	}

		//多图片上传
		function fileupload2(input,img,photoCover){
			var form = new FormData;
			var files = document.getElementById(input).files;
			console.log("files",files);
			for(var i=0; i< files.length; i++){
				i = i+1;
				var a ="file"+i;
				i = i-1;
				form.append(a,files[i]);
				
			}	
			console.log("form",form);
			$.ajax({
				url:URLS.SERVER_URL+URLS.upload,
				type:'POST',
				data:form,
				async: false,
						cache: false,
						contentType: false,
						processData: false,
						dataType: "json",
				success: function(data){
					console.log(data);
					

					var domian = data.image;
					for(var c=0; c< domian.length; c++){
						console.log("files",files);
					$("."+img).append("<span><img class='tpsrc' src='"+domian[c]+"'><i class='fa fa-fw fa-times del_img'></i></span>")
					}
					console.log($("#"+photoCover).val());
					if($("#"+photoCover).val()!=""){
					    $("#"+photoCover).val($("#"+photoCover).val()+","+domian);
					}else{
						$("#"+photoCover).val(domian);
					}
					console.log("photoCover:"+$("#"+photoCover).val());
				
				}
			});
		}



		//上传一对一
		function fileupload3(input,img,photoCover){
			var form = new FormData;
			var files = document.getElementById(input).files;
			console.log("files",files);
			for(var i=0; i< files.length; i++){
				i = i+1;
				var a ="file"+i;
				i = i-1;
				form.append(a,files[i]);
			}	
			console.log("form",form);
			$.ajax({
				url:URLS.SERVER_URL+URLS.upload,
				type:'POST',
				data:form,
				async: false,
						cache: false,
						contentType: false,
						processData: false,
						dataType: "json",
				success: function(data){
					console.log(data);

					var domian = data.image;
					for(var c=0; c< domian.length; c++){
						
						console.log("files",files);
					$("."+img).append("<p class='ydy'><span><img class='tpsrc' src='"+domian[c]+"'><i class='fa fa-fw fa-times del_img_ydy'></i></span><textarea placeholder='请输入文字描述' class='form-control ydy_textarea xd_introduction"+c+"'></textarea></p>")
					}
					console.log($("#"+photoCover).val());
					if($("#"+photoCover).val()!=""){
					    $("#"+photoCover).val($("#"+photoCover).val()+","+domian);
					}else{
						$("#"+photoCover).val(domian);
					}
					console.log("photoCover:"+$("#"+photoCover).val());
				
				}
			});
		}


			//删除图片一对一
			$(".content-wrapper").on("click", '.del_img_ydy', function() {
				var aa=$(this).siblings(".tpsrc").attr("src");
				var dd = $(this).parent().parent().parent().siblings(".tp_zong").val();
				console.log(dd);
				dd = dd.replace(aa,"");
				if(dd.charAt(0)==","){
				 dd=dd.substr(1,dd.length);
				}
				if(dd.substr(dd.length-1,1)==","){
				 dd = dd.substr(0, dd.length - 1);
				}
				$(this).parent().parent().parent().siblings(".tp_zong").val(dd);
				console.log($(this).parent().parent().siblings(".tp_zong").val());
				$(this).parent().parent().remove();
			 })

			//删除图片
			$(".content-wrapper").on("click", '.del_img', function() {
			   var aa=$(this).siblings(".tpsrc").attr("src");
			   var dd = $(this).parent().parent().siblings(".tp_zong").val();
			   console.log(dd);
			   dd = dd.replace(aa,"");
			   if(dd.charAt(0)==","){
				dd=dd.substr(1,dd.length);
			   }
			   if(dd.substr(dd.length-1,1)==","){
				dd = dd.substr(0, dd.length - 1);
			   }
			   $(this).parent().parent().siblings(".tp_zong").val(dd);
			   console.log($(this).parent().parent().siblings(".tp_zong").val());
			   $(this).parent().remove();
			})

			
			
	// function fileupload(input,img,photoCover){
	// 	var fileObj = document.getElementById(input).files[0];
	// 	$.ajax({
	// 		url:'https://jiancai.dcofcity.com/yj_jiancai/Upload/UploadVideo',
	// 		type:'POST',
	// 		async:true,
	// 		data:{
	// 			upfile:fileObj
	// 		},
	// 		dataType: "json",
			
	// 		success: function(data){
	// 			console.log(data);
	// 			var form = new FormData;
	// 			form.append("upfile",fileObj);
	// 			form.append("key",new Date().getTime()+".jpg");
	// 			form.append("token",data.t.token);
	// 			var domian=data.t.domain;
	// 			$.ajax({
	// 				url:'http://up.qiniu.com',
	// 				type: 'POST',
	// 				data:form,
	// 				async: false,
	// 				cache: false,
	// 				contentType: false,
	// 				processData: false,
	// 				dataType: "json",
	// 				success: function(data) {
	// 				   $("."+img).html("<img src='"+domian+"/"+data.key+"'/>")
	// 				   $("#"+photoCover).val(""+domian+"/"+data.key+"");
	// 				},
	// 			});
	// 		}
	//     });
	// }
	$("#lefile1").change(
	  function(){
		 fileupload("lefile1","imgfile1","photoCover1")
	  }
	)
	$("#bt_picture_product").change(
	  function(){
		 fileupload("bt_picture_product","img_picture_product","xz_picture_product")
	  }
	)
	$("#bt_picture_banner").change(
		function(){
		   fileupload("bt_picture_banner","img_picture_banner","xz_picture_banner")
		}
	);

	$("#bt_picture_banner2").change(
		function(){
		   fileupload("bt_picture_banner2","img_picture_banner2","xz_picture_banner2")
		}
	);

	$("#duotu1").change(
		function(){
		   fileupload3("duotu1","duotu_imgfile1","duotu_input1")
		}
	);

	$("#duotu2").change(
		function(){
		   fileupload3("duotu2","duotu_imgfile2","duotu_input2")
		}
	);

	$("#duotu3").change(
		function(){
		   fileupload3("duotu3","duotu_imgfile3","duotu_input3")
		}
	);

	$("#duotu4").change(
		function(){
			fileupload3("duotu4","duotu_imgfile4","duotu_input4")
		}
	);

	//退出登录
	$("#logout").click(function(){
		sessionStorage.removeItem("backUserInfo");
		window.location.href ='../../../login.html';
	});
})
