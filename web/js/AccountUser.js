(function(){
	var pagenum=10;//每页数量
	var curpage=1;//当前页面
	var totalPage=0;//总页数
	//获取列表
	function getlist(curpage,pagenum){
	   var phone=$("#cx_phone").val();
	   var account=$("#cx_account").val();
	   var user_id=$("#cx_user_id").val();
	   console.log(URLS.SERVER_URL+URLS.listUser+"?pageSize="+pagenum+"&pageNum="+curpage)
	   $.ajax({
			url:URLS.SERVER_URL+URLS.listUser,
			type:'POST',
			async:true,
			data:{
                pageSize:pagenum,
				pageNum:curpage,
				phone:phone,
				account:account,
				user_id:user_id
			},
			dataType: "json",
			success: function(data){
				console.log(data);
				dataset=data.data;
				if(dataset && dataset.length > 0){
				   var html = template('datalist',{dataset:dataset});
				   $("#example1").html(html);
				   totalPage=data.totalPage;
				   $(".pageinfo").html("当前"+curpage+"页/共"+totalPage+"页");
				   $(".curpage").val(curpage);
				   if(curpage==1){$(".prev").addClass("disabled")}else{$(".prev").removeClass("disabled")}
				   if(curpage==totalPage){$(".next").addClass("disabled")}else{$(".next").removeClass("disabled")}
				}else{
				   $("#example1").html("<tbody><tr><td>查不到数据</td></tr></tbody>");
				};
			}
	   });
	}

	var Page = {
		init:function(){
			this.render();
			this.event();
		},
		//绑定事件
		event:function(){
			//上一页
		    $(".prev").click(
			    function(){
					if(curpage>1){
				     	curpage=curpage-1;
					    getlist(curpage,pagenum);
					}
			    }
			)

			//下一页
		    $(".next").click(
			    function(){
					if(curpage<totalPage){
				     	curpage=curpage+1;
					    getlist(curpage,pagenum);
					}
			    }
			)

			//页码跳转
			$(".gopage").click(
			    function(){
					if($(".curpage").val()>=1&&$(".curpage").val()<=totalPage){
				    	curpage=parseInt($(".curpage").val());
					    getlist(curpage,pagenum)
					}else{
						$(".curpage").val(curpage);
					}
			    }
			)
			//搜索按钮
			$(".searchbtn").click(function(){
				curpage=1
				getlist(curpage,pagenum)
			})
			//删除按钮
			$(".content-wrapper").on("click", '.delete', function() {
				$("#modal-delete .modal-body").attr("data-id",$(this).attr("data-id"))
			})
			//确认删除
			$(".btndelete").click(function(){
			   var id=$("#modal-delete .modal-body").attr("data-id");
			   $.ajax({
					url:URLS.SERVER_URL+URLS.deleteUser,
					type:'POST',
					async:true,
					data:{
						id:id
					},
					dataType: "json",
					success: function(data){
						if(data.state === 0 ){
							$("#modal-delete").modal('hide');
							getlist(curpage,pagenum);
						}else{
						    alert("删除失败")	
						}
					}
			   });
		    })
			//新建修改按钮（企业）
			$(".content-wrapper").on("click", '.addupdate', function() {
				var id=$(this).attr("data-id");
				$("#photoCover1").val("");
				$(".imgfile1").html("");
				
				if(id!=null){
					$(".bianji").show();
					$("#id").val(id);
				  var obj=$(this).parent().parent().find("td");
				  $("#modal-addupdate .modal-title").html("修改");
				  $("#xj_account").val(obj.eq(1).html());
				  $("#xj_phone").val(obj.eq(7).html());
				  $("#xj_level").val(obj.eq(4).html());
				  $("#xj_nick_name").val(obj.eq(6).html());

				  $.ajax({
					url:URLS.SERVER_URL+URLS.detailUser,
					type:'POST',
					async:true,
					data:{
						id:id
					},
					dataType: "json",
					success: function(data){
						console.log(data);
						if(data.state === 0 ){

							$("#xj_password").val(data.data.password);
							if(data.data.picture!=null){
								$("#photoCover1").val(data.data.picture);
								$(".imgfile1").html("<img src='"+data.data.picture+"'/>");}
						}else{
							alert("操作失败")	
						}
					}
				 });
					 
					 
			   	  $("#xj_title").val(obj.eq(11).html());
				  $("#xj_recommend_order").val(obj.eq(10).html());
				  $(".imgfile1").html("<img src='"+$(this).attr("data-picture")+"'/>");
				  $("#xj_follow_count").val(obj.eq(3).html());
				  $("#xj_praise_count").val(obj.eq(8).html());
				  $("#xj_recommend_count").val(obj.eq(9).html());
				  $("#xj_complain_count").val(obj.eq(2).html());
				  $("#xj_money").val(obj.eq(5).html());

				}else{
					$(".bianji").hide();
				  $("#modal-addupdate .modal-title").html("新建")
				  $("#xj_account").val("");
				  $("#xj_phone").val("");
				  $("#xj_level").val("");
				  $("#xj_nick_name").val("");
			   	  $("#xj_password").val("");
			   	  $("#xj_title").val("");
				  $("#xj_recommend_order").val("");
				  $(".imgfile1").html("");
				  $("#xj_follow_count").val("");
				  $("#xj_praise_count").val("");
				  $("#xj_recommend_count").val("");
				  $("#xj_complain_count").val("");
				  $("#xj_money").val("");
				  
				  
				}
			})
			//确认修改按钮（企业）
			$(".btnaddupdate").click(function(){
				 var id=$("#id").val();
				 var account=$("#xj_account").val();
				 var phone=$("#xj_phone").val();
				 var level=$("#xj_level").val();
				 var nick_name=$("#xj_nick_name").val();
				 var password=$("#xj_password").val();
				 var title=$("#xj_title").val();
				 var recommend_order=$("#xj_recommend_order").val();
				 var picture=$("#photoCover1").val();
				 var follow_count=$("#xj_follow_count").val();
				 var praise_count=$("#xj_praise_count").val();
				 var recommend_count=$("#xj_recommend_count").val();
				 var complain_count=$("#xj_complain_count").val();
				 var money=$("#xj_money").val();
				
				//  if(phone==""){alert("请输入联系方式");return}
				 var data={}
				 if(id>0){
					 console.log(`${URLS.SERVER_URL+URLS.updateUser}?id=${id}&account=${account}&phone=${phone}&level=${level}&nick_name=${nick_name}&password=${password}&title=${title}&recommend_order=${recommend_order}&picture=${picture}&follow_count=${follow_count}&praise_count=${praise_count}&recommend_count=${recommend_count}&complain_count=${complain_count}&money=${money}`);
					 $.ajax({
						url:URLS.SERVER_URL+URLS.updateUser,
						type:'POST',
						async:true,
						data:{
							id:id,
							account:account,
							phone:phone,
							level:level,
							nick_name:nick_name,
							password:password,
							title:title,
							recommend_order:recommend_order,
							picture:picture,
							follow_count:follow_count,
							praise_count:praise_count,
							recommend_count:recommend_count,
							complain_count:complain_count,
							money:money
						},
						dataType: "json",
						success: function(data){
							console.log(data);
							if(data.state === 0 ){
								$("#modal-addupdate").modal('hide');
								getlist(curpage,pagenum);
							}else{
								alert("操作失败")	
							}
						}
				     });
				 }else{
					 $.ajax({
						url:URLS.SERVER_URL+URLS.insertUser,
						type:'POST',
						async:true,
						data:{
							account:account,
							nick_name:nick_name,
							title:title,
							password:password
						},
						dataType: "json",
						success: function(data){
							if(data.state === 0 ){
								$("#modal-addupdate").modal('hide');
								getlist(curpage,pagenum);
							}else{
								alert("操作失败")	
							}
						}
				     });
				 }
			})
			//编辑产品按钮
			$(".content-wrapper").on("click", '.product', function() {	
	            pagenum2=5;//每页数量
	            curpage2=1;//当前页面
	            totalPage2=0;//总页数
				var id=$(this).attr("data-id")
				$("#modal-product .modal-body").attr("data-id",id)
				getlist2(id,curpage2,pagenum2)
			})
			//新建修改按钮（产品）
			$(".content-wrapper").on("click", '.addupdatepro', function() {
				var id=$(this).attr("data-id");
				if(id!=null){
				  var obj=$(this).parent().parent().find("td");
				  $("#modal-addupdatepro .modal-title").html("修改产品");
				  $("#proid").val(id);
				  $("#productName").val(obj.eq(1).html());
				  $("#productType").val(obj.eq(2).html());
			   	  $("#productScore").val(obj.eq(3).html());
				}else{
				  $("#modal-addupdatepro .modal-title").html("新建产品")
				  $("#proid").val("");
				  $("#productName").val("");
				  $("#productType option:first").prop("selected", 'selected');
			   	  $("#productScore").val("");
				}
			})
			//确认修改按钮（产品）
			$(".btnaddupdatepro").click(function(){
				 var id=$("#proid").val();
				 var companyId=$("#modal-product .modal-body").attr("data-id");
				 var productName=$("#productName").val();
				 var productType=$("#productType").val();
				 var productScore=$("#productScore").val();
				 if(productName==""){alert("请输入产品名称");return}
				 if(id>0){
					 $.ajax({
						url:'https://jiancai.dcofcity.com/yj_jiancai/updateProduct',
						type:'POST',
						async:true,
						data:{
							id:id,
							companyId:companyId,
							productName:productName,
							productType:productType,
							productScore:productScore
						},
						dataType: "json",
						success: function(data){
							if(data.state === 0 ){
								$("#modal-addupdatepro").modal('hide');
								getlist2(companyId,curpage2,pagenum2);
							}else{
								alert("操作失败")	
							}
						}
				     });
				 }else{
					 $.ajax({
						url:'https://jiancai.dcofcity.com/yj_jiancai/insertProduct',
						type:'POST',
						async:true,
						data:{
							companyId:companyId,
							productName:productName,
							productType:productType,
							productScore:productScore
						},
						dataType: "json",
						success: function(data){
							if(data.state === 0 ){
								$("#modal-addupdatepro").modal('hide');
								getlist2(companyId,curpage2,pagenum2);
							}else{
								alert("操作失败")	
							}
						}
				     });
				 }
			})
			//删除按钮
			$(".content-wrapper").on("click", '.deletepro', function() {
				$("#modal-deletepro .modal-body").attr("data-id",$(this).attr("data-id"))
			})
			//确认删除
			$(".btndeletepro").click(function(){
			   var id=$("#modal-deletepro .modal-body").attr("data-id");
			   $.ajax({
					url:'https://jiancai.dcofcity.com/yj_jiancai/deleteProduct',
					type:'POST',
					async:true,
					data:{
						id:id
					},
					dataType: "json",
					success: function(data){
						if(data.state === 0 ){
							var id=$("#modal-product .modal-body").attr("data-id")
							$("#modal-deletepro").modal('hide');
							getlist2(id,curpage2,pagenum2);
						}else{
						    alert("删除失败")	
						}
					}
			   });
		    })
		},
		//渲染页面
		render:function(){
			getlist(curpage,pagenum)
		}
	};
	Page.init();
})();