(function(){
	var pagenum=10;//每页数量
	var curpage=1;//当前页面
	var totalPage=0;//总页数
	
	//获取列表
	function getlist(curpage,pagenum){
	   var user_id=$("#cx_user_id").val();
	   var post_id=$("#cx_post_id").val();
	   $.ajax({
			url:URLS.SERVER_URL+URLS.listReply,
			type:'POST',
			async:true,
			data:{
                pageSize:pagenum,
				pageNum:curpage,
				user_id:user_id,
				post_id:post_id,
			},
			dataType: "json",
			success: function(data){
				dataset=data.data;
				if(dataset && dataset.length > 0){
				   var html = template('datalist',{dataset:dataset});
				   $("#example1").html(html);
				   totalPage=data.totalPage;
				   $(".pageinfo").html("当前"+curpage+"页/共"+totalPage+"页");
				   $(".curpage").val(curpage);
				   if(curpage==1){$(".prev").addClass("disabled")}else{$(".prev").removeClass("disabled")}
				   if(curpage==totalPage){$(".next").addClass("disabled")}else{$(".next").removeClass("disabled")}
				}else{
				   $("#example1").html("<tbody><tr><td>查不到数据</td></tr></tbody>");
				};
			}
	   });
	}
	function getlist2(id,curpage,pagenum){
		$.ajax({
			url:'https://jiancai.dcofcity.com/yj_jiancai/findCommentList',
			type:'POST',
			async:true,
			data:{
				id:id,
				pageSize:pagenum2,
				pageNum:curpage2
			},
			dataType: "json",
			success: function(data){
				dataset=data.data;
				if(dataset && dataset.length > 0){
				   var html = template('datalist2',{dataset:dataset});
				   $("#example2").html(html);
				   totalPage2=data.totalPage;
				   $(".pageinfo2").html("当前"+curpage2+"页/共"+totalPage2+"页");
				   $(".curpage2").val(curpage2);
				   if(curpage2==1){$(".prev2").addClass("disabled")}else{$(".prev2").removeClass("disabled")}
				   if(curpage2==totalPage2){$(".next2").addClass("disabled")}else{$(".next2").removeClass("disabled")}
				}else{
				   $(".curpage2").val(1);
				   $(".pageinfo2").html("");
				   $("#example2").html("<tbody><tr><td>查不到数据</td></tr></tbody>");
				};
			}
	   });
	}
	var Page = {
		init:function(){
			this.render();
			this.event();
		},
		//绑定事件
		event:function(){
			//上一页
		    $(".prev").click(
			    function(){
					if(curpage>1){
				     	curpage=curpage-1;
					    getlist(curpage,pagenum);
					}
			    }
			)
		    $(".prev2").click(
			    function(){
					var id=$("#modal-product .modal-body").attr("data-id")
					if(curpage2>1){
				     	curpage2=curpage2-1;
					    getlist2(id,curpage2,pagenum2);
					}
			    }
			)
			//下一页
		    $(".next").click(
			    function(){
					if(curpage<totalPage){
				     	curpage=curpage+1;
					    getlist(curpage,pagenum);
					}
			    }
			)
		    $(".next2").click(
			    function(){
					var id=$("#modal-product .modal-body").attr("data-id")
					if(curpage2<totalPage2){
				     	curpage2=curpage2+1;
					    getlist2(id,curpage2,pagenum2);
					}
			    }
			)
			//页码跳转
			$(".gopage").click(
			    function(){
					if($(".curpage").val()>=1&&$(".curpage").val()<=totalPage){
				    	curpage=parseInt($(".curpage").val());
					    getlist(curpage,pagenum)
					}else{
						$(".curpage").val(curpage);
					}
			    }
			)
			$(".gopage2").click(
			    function(){
					var id=$("#modal-product .modal-body").attr("data-id")
					if($(".curpage2").val()>=1&&$(".curpage2").val()<=totalPage2){
				    	curpage2=parseInt($(".curpage2").val());
					    getlist2(id,curpage2,pagenum2)
					}else{
						$(".curpage2").val(curpage2);
					}
			    }
			)
			//搜索按钮
			$(".searchbtn").click(function(){
				curpage=1
				getlist(curpage,pagenum)
			})

			//全选
			$(".content").on("click", '.allcheck', function() {
				if ($(".allcheck").attr("checked")) { 
				    $(".allcheck").attr("checked", false)
					$("input[name=items]").each(function() {  
						$(this).attr("checked", false);  
					});  
				} else {  
				    $(".allcheck").attr("checked", true)
					$("input[name=items]").each(function() {  
						$(this).attr("checked", true);  
					});  
				}  
			})
			//删除按钮
			$(".content-wrapper").on("click", '.delete', function() {
				if($(this).attr("data-id")>0){
				   $("#modal-delete .modal-body").attr("data-id",$(this).attr("data-id"))
				   $("#modal-delete .modal-body").html("确认删除该数据吗？")
				}else{
				   $("#modal-delete .modal-body").removeAttr("data-id");
				   $("#modal-delete .modal-body").html("确认批量删除数据吗？")
				}
			})


			// //删除按钮
			// $(".content-wrapper").on("click", '.delete', function() {
			// 	$("#modal-delete .modal-body").attr("data-id",$(this).attr("data-id"))
			// })
			//确认删除
			$(".btndelete").click(function(){
			//    var id=$("#modal-delete .modal-body").attr("data-id");

			   var id=$("#modal-delete .modal-body").attr("data-id");
			   if(id>0){
				    id=$("#modal-delete .modal-body").attr("data-id");
			   }else{
				    id="";
					$("input[name=items]").each(function() {
						if ($(this).prop("checked")) {
							id += ","+$(this).val();
						}
					}); 
					id=id.substr(1)
			   }
			   if(id==""){
			     alert("请选择数据");
				 return;
			   }


			   $.ajax({
					url:URLS.SERVER_URL+URLS.deleteReply,
					type:'POST',
					async:true,
					data:{
						id:id
					},
					dataType: "json",
					success: function(data){
						if(data.state === 0 ){
							$("#modal-delete").modal('hide');
							getlist(curpage,pagenum);
						}else{
						    alert("删除失败")	
						}
					}
			   });
		    })
			//新建修改按钮（企业）
			$(".content-wrapper").on("click", '.addupdate', function() {
				var id=$(this).attr("data-id");
				if(id!=null){
				  var obj=$(this).parent().parent().find("td");
				  $("#modal-addupdate .modal-title").html("修改企业");
				  $("#id").val(id);
				  $("#companyName").val(obj.eq(1).html());
				  $("#address").val(obj.eq(2).html());
			   	  $("#phone").val(obj.eq(3).html().trim());
				  $("#companyProfile").val($(this).attr("data-companyProfile"));
				  $("#photoCover1").val($(this).attr("data-picture"));
				  $(".imgfile1").html("<img src='"+$(this).attr("data-picture")+"'/>")	
				  $("#score").val(obj.eq(4).html());
				  $("#isRecommend").val(obj.eq(5).html()=="是"?"1":"2");
				  $("#orderLevel").val(obj.eq(6).html());
				}else{
				  $("#modal-addupdate .modal-title").html("新建企业")
				  $("#id").val("");
				  $("#companyName").val("");
				  $("#address").val("");
			   	  $("#phone").val("");
			   	  $("#companyProfile").val("");
				  $("#photoCover1").val("");
				  $(".imgfile1").html("");
				  $("#lefile1").val("");
				  $("#score").val("");
				  $("#isRecommend option:first").prop("selected", 'selected');
				  $("#orderLevel").val(0);
				}
			})
			//确认修改按钮（企业）
			$(".btnaddupdate").click(function(){
				 var id=$("#id").val();
				 var companyName=$("#companyName").val();
				 var address=$("#address").val();
				 var phone=$("#phone").val();
				 var companyProfile=$("#companyProfile").val();
				 var picture=$("#photoCover1").val();
				 var score=$("#score").val();
				 var isRecommend=$("#isRecommend").val();
				 var orderLevel=$("#orderLevel").val();
				 if(companyName==""){alert("请输入企业名称");return}
				 if(address==""){alert("请输入地址");return}
				 if(phone==""){alert("请输入联系方式");return}
				 var data={}
				 if(id>0){
					 $.ajax({
						url:'https://jiancai.dcofcity.com/yj_jiancai/updateCompany',
						type:'POST',
						async:true,
						data:{
							id:id,
							companyName:companyName,
							address:address,
							phone:phone,
							companyProfile:companyProfile,
							picture:picture,
							score:score,
							isRecommend:isRecommend,
							orderLevel:orderLevel
						},
						dataType: "json",
						success: function(data){
							if(data.state === 0 ){
								$("#modal-addupdate").modal('hide');
								getlist(curpage,pagenum);
							}else{
								alert("操作失败")	
							}
						}
				     });
				 }else{
					 $.ajax({
						url:'https://jiancai.dcofcity.com/yj_jiancai/insertCompany',
						type:'POST',
						async:true,
						data:{
							companyName:companyName,
							address:address,
							phone:phone,
							companyProfile:companyProfile,
							picture:picture,
							score:score,
							isRecommend:isRecommend,
							orderLevel:orderLevel},
						dataType: "json",
						success: function(data){
							if(data.state === 0 ){
								$("#modal-addupdate").modal('hide');
								getlist(curpage,pagenum);
							}else{
								alert("操作失败")	
							}
						}
				     });
				 }
			})
			//编辑产品按钮
			$(".content-wrapper").on("click", '.product', function() {	
	            pagenum2=5;//每页数量
	            curpage2=1;//当前页面
	            totalPage2=0;//总页数
				var id=$(this).attr("data-id")
				$("#modal-product .modal-body").attr("data-id",id)
				getlist2(id,curpage2,pagenum2)
			})
			//新建修改按钮（产品）
			$(".content-wrapper").on("click", '.addupdatepro', function() {
				var id=$(this).attr("data-id");
				if(id!=null){
				  var obj=$(this).parent().parent().find("td");
				  $("#modal-addupdatepro .modal-title").html("修改产品");
				  $("#proid").val(id);
				  $("#productName").val(obj.eq(1).html());
				  $("#productType").val(obj.eq(2).html());
			   	  $("#productScore").val(obj.eq(3).html());
				}else{
				  $("#modal-addupdatepro .modal-title").html("新建产品")
				  $("#proid").val("");
				  $("#productName").val("");
				  $("#productType option:first").prop("selected", 'selected');
			   	  $("#productScore").val("");
				}
			})
			//确认修改按钮（产品）
			$(".btnaddupdatepro").click(function(){
				 var id=$("#proid").val();
				 var companyId=$("#modal-product .modal-body").attr("data-id");
				 var productName=$("#productName").val();
				 var productType=$("#productType").val();
				 var productScore=$("#productScore").val();
				 if(productName==""){alert("请输入产品名称");return}
				 if(id>0){
					 $.ajax({
						url:'https://jiancai.dcofcity.com/yj_jiancai/updateProduct',
						type:'POST',
						async:true,
						data:{
							id:id,
							companyId:companyId,
							productName:productName,
							productType:productType,
							productScore:productScore
						},
						dataType: "json",
						success: function(data){
							if(data.state === 0 ){
								$("#modal-addupdatepro").modal('hide');
								getlist2(companyId,curpage2,pagenum2);
							}else{
								alert("操作失败")	
							}
						}
				     });
				 }else{
					 $.ajax({
						url:'https://jiancai.dcofcity.com/yj_jiancai/insertProduct',
						type:'POST',
						async:true,
						data:{
							companyId:companyId,
							productName:productName,
							productType:productType,
							productScore:productScore
						},
						dataType: "json",
						success: function(data){
							if(data.state === 0 ){
								$("#modal-addupdatepro").modal('hide');
								getlist2(companyId,curpage2,pagenum2);
							}else{
								alert("操作失败")	
							}
						}
				     });
				 }
			})
			//删除按钮
			$(".content-wrapper").on("click", '.deletepro', function() {
				$("#modal-deletepro .modal-body").attr("data-id",$(this).attr("data-id"))
			})
			//确认删除
			$(".btndeletepro").click(function(){
			   var id=$("#modal-deletepro .modal-body").attr("data-id");
			   $.ajax({
					url:'https://jiancai.dcofcity.com/yj_jiancai/deleteProduct',
					type:'POST',
					async:true,
					data:{
						id:id
					},
					dataType: "json",
					success: function(data){
						if(data.state === 0 ){
							var id=$("#modal-product .modal-body").attr("data-id")
							$("#modal-deletepro").modal('hide');
							getlist2(id,curpage2,pagenum2);
						}else{
						    alert("删除失败")	
						}
					}
			   });
		    })
		},
		//渲染页面
		render:function(){
			getlist(curpage,pagenum)
		}
	};
	Page.init();
})();