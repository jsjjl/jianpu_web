(function(){
	var pagenum=10;//每页数量
	var curpage=1;//当前页面
	var totalPage=0;//总页数
	//获取列表
	function getlist(curpage,pagenum){
	   var order=$("#cx_order").val();
	   var type=$("#cx_type").val();
	   var father_id=$("#cx_father_id").val();
	   var user_id=$("#cx_user_id").val();
	   $.ajax({
			url:URLS.SERVER_URL+URLS.listPost,
			type:'POST',
			async:true,
			data:{
                pageSize:pagenum,
				pageNum:curpage,
				order:order,
				type:type,
				father_id:father_id,
				user_id:user_id,
			},
			dataType: "json",
			success: function(data){
				dataset=data.data;
				if(dataset && dataset.length > 0){
				   var html = template('datalist',{dataset:dataset});
				   $("#example1").html(html);
				   totalPage=data.totalPage;
				   $(".pageinfo").html("当前"+curpage+"页/共"+totalPage+"页");
				   $(".curpage").val(curpage);
				   if(curpage==1){$(".prev").addClass("disabled")}else{$(".prev").removeClass("disabled")}
				   if(curpage==totalPage){$(".next").addClass("disabled")}else{$(".next").removeClass("disabled")}
				}else{
				   $("#example1").html("<tbody><tr><td>查不到数据</td></tr></tbody>");
				};
			}
	   });
	}

	var Page = {
		init:function(){
			this.render();
			this.event();
		},
		//绑定事件
		event:function(){
			//上一页
		    $(".prev").click(
			    function(){
					if(curpage>1){
				     	curpage=curpage-1;
					    getlist(curpage,pagenum);
					}
			    }
			)

			//下一页
		    $(".next").click(
			    function(){
					if(curpage<totalPage){
				     	curpage=curpage+1;
					    getlist(curpage,pagenum);
					}
			    }
			)
		    
			//页码跳转
			$(".gopage").click(
			    function(){
					if($(".curpage").val()>=1&&$(".curpage").val()<=totalPage){
				    	curpage=parseInt($(".curpage").val());
					    getlist(curpage,pagenum)
					}else{
						$(".curpage").val(curpage);
					}
			    }
			)

			//搜索按钮
			$(".searchbtn").click(function(){
				curpage=1
				getlist(curpage,pagenum)
			})
			//删除按钮
			$(".content-wrapper").on("click", '.delete', function() {
				$("#modal-delete .modal-body").attr("data-id",$(this).attr("data-id"))
			})
			//确认删除
			$(".btndelete").click(function(){
			   var id=$("#modal-delete .modal-body").attr("data-id");
			   $.ajax({
					url:URLS.SERVER_URL+URLS.deletePost,
					type:'POST',
					async:true,
					data:{
						id:id
					},
					dataType: "json",
					success: function(data){
						if(data.state === 0 ){
							$("#modal-delete").modal('hide');
							getlist(curpage,pagenum);
						}else{
						    alert("删除失败")	
						}
					}
			   });
		    })
			//新建修改 tw
			$(".content-wrapper").on("click", '.addupdate', function() {
				var id=$(this).attr("data-id");
				$("#duotu_input1,#duotu_input2,#duotu_input3,#xz_complain_count").val("");
				$(".duotu_imgfile1,.duotu_imgfile2,.duotu_imgfile3,.img_picture_product,.img_picture_banner").html("");
				if(id!=null){
				  $("#modal-addupdate .modal-title").html("修改");
				  $("#id").val(id);

				  console.log(`${URLS.SERVER_URL}${URLS.detailPost}?id=${id}`);
				  $.ajax({
					url:URLS.SERVER_URL+URLS.detailPost,
					type:'POST',
					async:true,
					data:{
						id:id
					},
					dataType: "json",
					success: function(data){
						console.log(data);
						if(data.state === 0 ){
							$("#xz_post_name").val(data.data.post_name);
							$("#xz_reason").val(data.data.reason);
							$("#xz_title").val(data.data.title);
							$("#xz_adapt_to").val(data.data.adapt_to);
							$("#xz_attention").val(data.data.attention);
							$("#xz_area").val(data.data.area);
							$("#xz_experience_address").val(data.data.experience_address);
							$("#xz_read_count").val(data.data.read_count);
							$("#xz_favorite_count").val(data.data.favorite_count);
							$("#xz_complain_count").val(data.data.complain_count)
							$("#xz_reply_count").val(data.data.reply_count);
							$("#xz_user_id").val(data.data.user_id);
							$("#xz_picture_product").val(data.data.picture_product);
							$("#xz_picture_banner").val(data.data.picture_banner);
							
							$(".img_picture_product").html("<img src='"+data.data.picture_product+"'/>");
							$(".img_picture_banner").html("<img src='"+data.data.picture_banner+"'/>");

							if(data.data.introduction!=null||data.data.introduction!=undefined){
								var aa1=data.data.introduction;
								var ydy1="";
								var ydyinput1="";
								
								for(ia=0;ia<data.data.introduction.length;ia++){
									ydy1 +='<p class="ydy"><span><img class="tpsrc" src="'+
									aa1[ia].image+'"><i class="fa fa-fw fa-times del_img_ydy"></i></span><textarea class="form-control ydy_textarea xd_introduction'+ia+'">'+
									aa1[ia].write+'</textarea></p>';
									ydyinput1 += aa1[ia].image +',';
								}

								ydyinput1 = ydyinput1.replace(",,",",");
								if(ydyinput1.charAt(0)==","){
									ydyinput1=ydyinput1.substr(1,ydyinput1.length);
								}
								if(ydyinput1.substr(ydyinput1.length-1,1)==","){
									ydyinput1 = ydyinput1.substr(0, ydyinput1.length - 1);
								}

								$("#duotu_input1").val(ydyinput1);
								$(".duotu_imgfile1").html(ydy1);

							}

							if(data.data.environment!=null||data.data.environment!=undefined){
								var aa2=data.data.environment;
								var ydy2="";
								var ydyinput2="";
								
								for(ib=0;ib<data.data.environment.length;ib++){
									ydy2 +='<p class="ydy"><span><img class="tpsrc" src="'+
									aa2[ib].image+'"><i class="fa fa-fw fa-times del_img_ydy"></i></span><textarea class="form-control ydy_textarea xd_introduction'+ib+'">'+
									aa2[ib].write+'</textarea></p>';
									ydyinput2 += aa2[ib].image +',';
								}

								ydyinput2 = ydyinput2.replace(",,",",");
								if(ydyinput2.charAt(0)==","){
									ydyinput2=ydyinput2.substr(1,ydyinput2.length);
								}
								if(ydyinput2.substr(ydyinput2.length-1,1)==","){
									ydyinput2 = ydyinput2.substr(0, ydyinput2.length - 1);
								}

								$("#duotu_input2").val(ydyinput2);
								$(".duotu_imgfile2").html(ydy2);
							}

							
							if(data.data.goods!=null||data.data.goods!=undefined){
								var aa3=data.data.goods;
								var ydy3="";
								var ydyinput3="";
								
								for(ic=0;ic<data.data.goods.length;ic++){
									ydy3 +='<p class="ydy"><span><img class="tpsrc" src="'+
									aa3[ic].image+'"><i class="fa fa-fw fa-times del_img_ydy"></i></span><textarea class="form-control ydy_textarea xd_introduction'+ic+'">'+
									aa3[ic].write+'</textarea></p>';
									ydyinput3 += aa3[ic].image +',';
								}

								ydyinput3 = ydyinput3.replace(",,",",");
								if(ydyinput3.charAt(0)==","){
									ydyinput3=ydyinput3.substr(1,ydyinput3.length);
								}
								if(ydyinput3.substr(ydyinput3.length-1,1)==","){
									ydyinput3 = ydyinput3.substr(0, ydyinput3.length - 1);
								}

								$("#duotu_input3").val(ydyinput3);
								$(".duotu_imgfile3").html(ydy3);
							}
							
							
						}else{
							alert("操作失败")	
						}
					}
				 });

				

				}else{
				  $("#modal-addupdate .modal-title").html("新增");
				  $("#id").val("");
				  $("#xz_post_name").val("");
				  $("#xz_reason").val("");
				  $("#xz_title").val("");
				  $("#xz_adapt_to").val("");
				  $("#xz_attention").val("");
				  $("#xz_area").val("");
				  $("#xz_experience_address").val("");
				  $("#xz_read_count").val("");
				  $("#xz_favorite_count").val("");
				  $("#xz_reply_count").val("");
				  $("#xz_user_id").val("");
				  




				  $("#duotu_input1,#duotu_input2,#duotu_input3,#xz_picture_product,#xz_picture_banner").val("");
				}
			})
			//确认 tw
			$(".btnaddupdate").click(function(){
				 var id=$("#id").val();
				 var post_name=$("#xz_post_name").val();
				 var reason=$("#xz_reason").val();
				 var title=$("#xz_title").val();
				 var adapt_to=$("#xz_adapt_to").val();
				 var attention=$("#xz_attention").val();
				 var area=$("#xz_area").val();
				 var experience_address=$("#xz_experience_address").val();
				 var read_count=$("#xz_read_count").val();
				 var favorite_count=$("#xz_favorite_count").val();
				 var complain_count=$("#xz_complain_count").val();
				 var reply_count=$("#xz_reply_count").val();
				 var user_id=$("#xz_user_id").val();

				 var picture_product=$("#xz_picture_product").val();
				 var picture_banner=$("#xz_picture_banner").val();


				 var atr1 =$(".duotu_imgfile1 p");
				 var pplb1 = "";
				 console.log("atr1:"+atr1);
				 if(atr1.length != 0){
				 
				 // var atr = atr.split(',');
 
					 for(var i1=0;i1<atr1.length;i1++){
						 var cl1 = '.xd_introduction'+i1;
						 console.log(cl1);
						 console.log($(cl1).val());
						 console.log(atr1[i1]);
 
						 pplb1 += '{"write":"'+$(atr1[i1]).find(".ydy_textarea").val()+'","image":"'+$(atr1[i1]).find(".tpsrc").attr("src")+'"},';
						 
					 }
 
					 pplb1="["+pplb1+"]";
 
					 console.log("pplb:"+ pplb1);
					 console.log(typeof pplb1);
				 }
 
				 alert(pplb1);
				  

				 var atr2 =$(".duotu_imgfile2 p");
				 var pplb2 = "";
				 console.log("atr2:"+atr2);
				 if(atr2.length != 0){
				 
				 // var atr = atr.split(',');
 
					 for(var i2=0;i2<atr2.length;i2++){
						 var cl2 = '.xd_introduction'+i2;
						 console.log(cl2);
						 console.log($(cl2).val());
						 console.log(atr2[i2]);
 
						 pplb2 += '{"write":"'+$(atr2[i2]).find(".ydy_textarea").val()+'","image":"'+$(atr2[i2]).find(".tpsrc").attr("src")+'"},';
						 
					 }
 
					 pplb2="["+pplb2+"]";
 
					 console.log("pplb:"+ pplb2);
					 console.log(typeof pplb2);
				 }
 
				 alert(pplb2);



				var atr3 =$(".duotu_imgfile3 p");
				var pplb3 = "";
				console.log("atr3:"+atr3);
				if(atr3.length != 0){
				
				// var atr = atr.split(',');

					for(var i3=0;i3<atr3.length;i3++){
						var cl3 = '.xd_introduction'+i3;
						console.log(cl3);
						console.log($(cl3).val());
						console.log(atr3[i3]);

						pplb3 += '{"write":"'+$(atr3[i3]).find(".ydy_textarea").val()+'","image":"'+$(atr3[i3]).find(".tpsrc").attr("src")+'"},';
						
					}

					pplb3="["+pplb3+"]";

					console.log("pplb:"+ pplb3);
					console.log(typeof pplb3);
				}

				alert(pplb3);

				
				
				 var data={}
				 if(id>0){
					console.log(`${URLS.SERVER_URL}${URLS.updatePost}?id=${id}&post_name=${post_name}&reason=${reason}&title=${title}&adapt_to=${adapt_to}&attention=${attention}&area=${area}&experience_address=${experience_address}&read_count=${read_count}&reply_count=${reply_count}&user_id=${user_id}&picture_product=${picture_product}&picture_banner=${picture_banner}&complain_count=${complain_count}`);
					 $.ajax({
						url:URLS.SERVER_URL+URLS.updatePost,
						type:'POST',
						async:true,
						data:{
							id:id,
							post_name:post_name,
							reason:reason,
							title:title,
							adapt_to:adapt_to,
							attention:attention,
							area:area,
							experience_address:experience_address,
							read_count:read_count,
							favorite_count:favorite_count,
							complain_count:complain_count,
							reply_count:reply_count,
							user_id:user_id,
							picture_product:picture_product,
							picture_banner:picture_banner,
							introduction:pplb1,
							environment:pplb2,
							goods:pplb3
						},
						dataType: "json",
						success: function(data){
							console.log(data);
							if(data.state === 0 ){
								$("#modal-addupdate").modal('hide');
								getlist(curpage,pagenum);
							}else{
								alert("操作失败")	
							}
						}
				     });
				 }else{
					console.log(`${URLS.SERVER_URL}${URLS.insertPost}?post_name=${post_name}&reason=${reason}&title=${title}&adapt_to=${adapt_to}&attention=${attention}&area=${area}&experience_address=${experience_address}&read_count=${read_count}&reply_count=${reply_count}&user_id=${user_id}&picture_product=${picture_product}&picture_banner=${picture_banner}`);
					 $.ajax({
						url:URLS.SERVER_URL+URLS.insertPost,
						type:'POST',
						async:true,
						data:{
							post_name:post_name,
							reason:reason,
							title:title,
							adapt_to:adapt_to,
							attention:attention,
							area:area,
							experience_address:experience_address,
							read_count:read_count,
							favorite_count:favorite_count,
							complain_count:complain_count,
							reply_count:reply_count,
							user_id:user_id,
							picture_product:picture_product,
							picture_banner:picture_banner,
							introduction:pplb1,
							environment:pplb2,
							goods:pplb3
						},
						dataType: "json",
						success: function(data){
							console.log(data);
							if(data.state === 0 ){
								$("#modal-addupdate").modal('hide');
								getlist(curpage,pagenum);
							}else{
								alert("操作失败")	
							}
						}
				     });
				 }
			})


			//新建修改 xd
			$(".content-wrapper").on("click", '.addupdatepro', function() {
				var id=$(this).attr("data-id");
				var fid=$(this).attr("data-fid");
				$(".img_picture_banner2").html("");
				$("#xz_picture_banner2").val("");

				$(".duotu_imgfile4").html("");
				$("#duotu_input4,#xd_id,#xd_fid").val("");

				if(id!=null){
				  $("#xd_id").val(id);
				  $("#modal-addupdatepro .modal-title").html("修改");
				  console.log(`${URLS.SERVER_URL}${URLS.detailPost}?id=${id}`);
				  $.ajax({
					url:URLS.SERVER_URL+URLS.detailPost,
					type:'POST',
					async:true,
					data:{
						id:id
					},
					dataType: "json",
					success: function(data){
						console.log(data);
						if(data.state === 0 ){
							$("#xd_post_name").val(data.data.post_name);
							$("#xd_experience_address").val(data.data.experience_address);
							$("#xd_reason").val(data.data.reason);
							$("#xd_father_id").val(data.data.father_id);
							$("#xd_user_id").val(data.data.user_id);
							$("#xd_read_count").val(data.data.read_count);
							$("#xd_favorite_count").val(data.data.favorite_count);
							$("#xd_complain_count").val(data.data.complain_count);
							$("#xd_reply_count").val(data.data.reply_count);
							if(data.data.picture_banner!=null||data.data.picture_banner!=undefined){
								$(".img_picture_banner2").html("<img src='"+data.data.picture_banner+"'/>");
								$("#xz_picture_banner2").val(data.data.picture_banner);
							}

				
							if(data.data.introduction!=null||data.data.introduction!=undefined){
								var aa=data.data.introduction;
								var ydy="";
								var ydyinput="";
								
								for(i=0;i<data.data.introduction.length;i++){
									ydy +='<p class="ydy"><span><img class="tpsrc" src="'+
									aa[i].image+'"><i class="fa fa-fw fa-times del_img_ydy"></i></span><textarea class="form-control ydy_textarea xd_introduction'+i+'">'+
									aa[i].write+'</textarea></p>';
									ydyinput += aa[i].image +',';
								}

								ydyinput = ydyinput.replace(",,",",");
								if(ydyinput.charAt(0)==","){
									ydyinput=ydyinput.substr(1,ydyinput.length);
								}
								if(ydyinput.substr(ydyinput.length-1,1)==","){
									ydyinput = ydyinput.substr(0, ydyinput.length - 1);
								}

								$("#duotu_input4").val(ydyinput);
								$(".duotu_imgfile4").html(ydy);
							}
							

						}else{
							alert("操作失败")	
						}
					}
				 });

				
				}else{
				  $("#xd_fid").val(fid);
				  $("#modal-addupdatepro .modal-title").html("新增")
				  $("#xd_post_name").val("");
				  $("#xd_experience_address").val("");
				  $("#xd_reason").val("");
				  $("#xd_father_id").val(fid);
				  $("#xd_user_id").val("");

				  $(".duotu_imgfile4").html("");
				  $("#duotu_input4").val("");
				  $("#xd_read_count").val("");
				  $("#xd_favorite_count").val("");
				  $("#xd_complain_count").val("");
				  $("#xd_reply_count").val("");
				  
				}
			})
			//确认 xd
			$(".btnaddupdatepro").click(function(){
				 var id=$("#xd_id").val();
				 var xid=$("#xd_fid").val();

				 var post_name=$("#xd_post_name").val();
				 var experience_address=$("#xd_experience_address").val();
				 var reason=$("#xd_reason").val();
				 var picture_banner=$("#xz_picture_banner2").val();
				 
				 var user_id=$("#xd_user_id").val();
				 var read_count=$("#xd_read_count").val();
				 var favorite_count=$("#xd_favorite_count").val();
				 var complain_count=$("#xd_complain_count").val();
				 
				 var reply_count=$("#xd_reply_count").val();

				 	// [{"write":"这儿是文字描述","image":"这里是图片url"},{"write":"描述","image":""}]
					// var atr = $("#duotu_input4").val();

					var atr =$(".duotu_imgfile4 p");
					var pplb = "";
					console.log("atr:"+atr);
					if(atr.length != 0){
					
					// var atr = atr.split(',');

						for(var i=0;i<atr.length;i++){
							var cl = '.xd_introduction'+i;
							console.log(cl);
							console.log($(cl).val());
							console.log(atr[i]);

							pplb += '{"write":"'+$(atr[i]).find(".ydy_textarea").val()+'","image":"'+$(atr[i]).find(".tpsrc").attr("src")+'"},';
							
						}

						pplb="["+pplb+"]";

						console.log("pplb:"+ pplb);
						console.log(typeof pplb);
					}
					 

				alert(pplb);
				 if(id>0){
					console.log(`${URLS.SERVER_URL}${URLS.updatePost}?id=${id}&post_name=${post_name}&experience_address=${experience_address}&reason=${reason}&picture_banner=${picture_banner}&user_id=${user_id}&introduction=pplb`);

					 $.ajax({
						url:URLS.SERVER_URL+URLS.updatePost,
						type:'POST',
						async:true,
						data:{
							id:id,
							post_name:post_name,
							experience_address:experience_address,
							reason:reason,
							picture_banner:picture_banner,
							user_id:user_id,
							read_count:read_count,
							favorite_count:favorite_count,
							complain_count:complain_count,
							reply_count:reply_count,
							introduction:pplb

						},
						dataType: "json",
						success: function(data){
							console.log(data);
							if(data.state === 0 ){
								$("#modal-addupdatepro").modal('hide');
								getlist(curpage,pagenum);
							}else{
								alert("操作失败")	
							}
						}
				     });
				 }else{
					var father_id=xid;
					console.log(`${URLS.SERVER_URL}${URLS.insertPostChild}?father_id=${father_id}&post_name=${post_name}&experience_address=${experience_address}&reason=${reason}&picture_banner=${picture_banner}&user_id=${user_id}&type=1`);
					 $.ajax({
						url:URLS.SERVER_URL+URLS.insertPostChild,
						type:'POST',
						async:true,
						data:{
							father_id:father_id,
							post_name:post_name,
							experience_address:experience_address,
							reason:reason,
							picture_banner:picture_banner,
							user_id:user_id,
							type:1,
							read_count:read_count,
							favorite_count:favorite_count,
							complain_count:complain_count,
							reply_count:reply_count,
							introduction:pplb
						},
						dataType: "json",
						success: function(data){
							console.log(data);
							if(data.state === 0 ){
								$("#modal-addupdatepro").modal('hide');
								getlist(curpage,pagenum);
							}else{
								alert("操作失败")	
							}
						}
				     });
				 }
			})
			
			
		},
		//渲染页面
		render:function(){
			getlist(curpage,pagenum)
		}
	};
	Page.init();
})();