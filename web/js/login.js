(function(){
	var Page = {
		init:function(){
			this.render();
			this.event();
		},
		//绑定事件
		event:function(){$(".btnlogin").click(function(){
				var accountName=$(".accountName").val()
				var password=$(".password").val()
				if(accountName==""){alert("请输入用户名");return}
				if(password==""){alert("请输入密码");return}

				// var url = URLS.SERVER_URL + URLS.login;
				// var my={};
				// my["account"] = accountName;
				// my["password"] = password;
				// var data = my;
				// var type = "POST";
				// //成功回调函数
				// var success = function(data){
				// 		if(data.state === 0 ){
				// 			var user=JSON.stringify(data.account); 
				// 			sessionStorage.setItem("backUserInfo", user);
				// 			window.location.href ='web/html/AccountManagement/AccountUser.html';
				// 		}else{
				// 			alert(data.msg)	
				// 		}     
				// };
				// ajaxPost(url,data,type,success);
		
				$.ajax({
					url:URLS.SERVER_URL + URLS.login,
					type:'GET',
					async:true,
					data:{
						account:accountName,
						password:password
					},
					dataType: "json",
					success: function(data){
						if(data.state === 0 ){
							console.log(data)
							var user=JSON.stringify(data.data); 
							sessionStorage.setItem("backUserInfo", user);
							window.location.href ='web/html/AccountManagement/AccountUser.html';
						}else{
							alert(data.msg)	
						}
					}
				 });
			})
		},
		//渲染页面
		render:function(){
		}
	};
	Page.init();
})();